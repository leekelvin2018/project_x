import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {AdministratorProfileService} from '../shared/admin.profile.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-administrator-profile',
  templateUrl: 'administrator-profile.component.html',
  styleUrls: ['administrator-profile.component.css']
})
export class AdministratorProfileComponent implements OnInit, OnDestroy {
  public placeholder = 'this is admin profile page';
  public profileSubscriber: Subscription = new Subscription();
  public offenseSubscriber: Subscription = new Subscription();

  public firsName;
  public lastName;
  public offenses;
  constructor(
    private adProfile: AdministratorProfileService,
    private router: Router
  ) {}
  public display() {
    this.profileSubscriber = this.adProfile.getProfile().subscribe(
      {next: (res) => {
          console.log(res.profiles[0].authority);
          this.firsName = res.first_name;
          this.lastName = res.last_name;
        },
        complete: () => {},
        error: (e) => {
          console.log(e);
          this.router.navigate(['login']);
        }
      }
    );
  }


  public offenseCounts() {
    this.offenseSubscriber = this.adProfile.getOffenseCount().subscribe(
      {
        next: (res) => { this.offenses = res; },
        complete: () => {},
        error: (e) => {console.log(e); }
      }
    );
  }

  public ngOnInit(): void {
    this.display();
    this.offenseCounts();
  }

  ngOnDestroy(): void {
    this.profileSubscriber.unsubscribe();
    this.offenseSubscriber.unsubscribe();
  }
}
