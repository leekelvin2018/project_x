import {Component, OnDestroy, OnInit} from '@angular/core';
import {AdminReportService} from '../shared/admin.report.service';
import {Subscription} from 'rxjs';
import {Router} from '@angular/router';

@Component({
  selector: 'app-administrator-clock-in',
  templateUrl: 'administrator-clock-in.component.html',
  styleUrls: ['administrator-clock-in.css']
})
export  class AdministratorClockInComponent implements OnInit, OnDestroy {
  public placeholder = Date.now();
  public message = '';
  private clockInSubscriber: Subscription = new Subscription();
  private clockOutSubscriber: Subscription = new Subscription();
  private interval;
  constructor(
    private ars: AdminReportService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.interval = setInterval(
      () => {this.placeholder = Date.now(); },
      1000
    );
    // document.getElementById('clock_container').addEventListener('click', (event) => {
    //   console.log(event);
    //   event.preventDefault();
    //   event.stopImmediatePropagation();
    // }, true);
  }

  public onClockIn() {
    console.log('clock in called');
    this.clockInSubscriber = this.ars.clockIn().subscribe(
      {
        next: (val) => {
          console.log(val);
          this.message = val.report; },
        complete: () => {},
        error: (e) => {
          console.log(e);
          this.router.navigate(['login']);
        }
      }
    );
  }

  public onClockOut() {
    this.clockOutSubscriber = this.ars.clockOut().subscribe(
      {
        next: (val) => {
          console.log(val);
          this.message = val.report;
        },
        complete: () => {},
        error: (e) => {
          console.log(e);
          this.router.navigate(['login']);
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.clockOutSubscriber.unsubscribe();
    this.clockInSubscriber.unsubscribe();
    clearTimeout(this.interval);
  }
}
