import {TestBed, async, ComponentFixture} from '@angular/core/testing';
import {OperatorMessageComponent} from './operatorMessage.component';
import {DebugElement} from '@angular/core';
import {MessageService} from '../shared/message.service';
import {Observable, of} from 'rxjs';
import {HttpClient} from '@angular/common/http';
describe('OperatorMessageComponent', () => {
  let component: OperatorMessageComponent;
  let fixture: ComponentFixture<OperatorMessageComponent>;
  let de: DebugElement;
  let ss: MessageService;
  let spy: any;
  beforeEach(async( () => {
    ss = new MessageService(null);
    TestBed.configureTestingModule({
      declarations: [OperatorMessageComponent],
      providers: [{provide: MessageService, useValue: ss}]
    }).compileComponents();
    }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OperatorMessageComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement;
    }
  );
  it('should be created', () => {
    expect(component).toBeTruthy();
  });
  it('should have sent state on false', () => {
    expect(component.sendStatus).toBeFalsy();
  });
  it('should have no notification', () => {
    expect(component.notification).toBeFalsy();
  });
  it('should not be waiting for response', () => {
    expect(component.waitingForResponse).toBeFalsy();
  });
  it('should have false contactObtained flag', () => {
    expect(component.contactObtained).toBeFalsy();
  });
  it('should have empty contact array', () => {
    expect(component.contacts).toBe([]);
  });
  it('should have no first name', () => {
    expect(component.firstname).toBeFalsy();
  });
  it('should have no last name', () => {
    expect(component.lastname).toBeFalsy();
  });
  it('should have no email address', () => {
    expect(component.email).toBeFalsy();
  });
  it('should have an empty subscription', () => {
    expect(component.emailSubscription).toBeTruthy();
    expect(component.contactSubscription).toBeTruthy();
  });
  it('should have predefined column names', () => {
    expect(component.displayCols).toBe(['email', 'firstname', 'lastname']);
  });
  it('should initialize', () => {
    expect(component.contactEmail).toBe('click an email address');
  });
  it('should return a list' , () => {
    spy = spyOn(ss, 'requestContact').and.returnValue(
      [{firstname: 'first', lastname: 'last', email: 'email', error: 'none'}]);
    expect(component.getContact()).toBeTruthy();
  });
});
