import {Component, OnDestroy, OnInit} from '@angular/core';
import {MessageService} from '../shared/message.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-operator-message',
  templateUrl: 'operatorMessage.component.html',
  styleUrls: ['operatorMessage.component.css']
})
export class OperatorMessageComponent implements OnInit, OnDestroy {
public sendStatus = false;
public notification;
public waitingForResponse = false;
public contactObtained = false;
public contacts = [];
public firstname;
public lastname;
public email;
public emailSubscription: Subscription = new Subscription();
public contactSubscription: Subscription = new Subscription();
public displayCols = ['email', 'firstname', 'lastname'];
public contactEmail = 'click an email address';
  constructor(
    protected ms: MessageService
  ) {}
  public testWebSock() {
    this.sendStatus = true;
    this.notification = 'attempting to deliver';
    this.waitingForResponse = true;
    const email = (document.getElementById('email') as HTMLInputElement).value;
    const subject = (document.getElementById('subject') as HTMLInputElement).value;
    const message = (document.getElementById('emailContent')as HTMLInputElement).value;
    if (subject.length == 0 || subject == null) {
      this.emailSubscription = this.ms.requestInformation(email, message).subscribe(
        {
          next: val => {
            console.log(val);
            if (val) {
              this.notification = 'delivered';
              this.waitingForResponse = false;
            } else {
              this.notification = 'deliver failed';
              this.waitingForResponse = false;
            }
            },
          complete: () => {},
          error: (e) => {
            console.log(e);
            this.notification = 'deliver failed';
            this.waitingForResponse = false;}
        }
      );
    } else {
      this.emailSubscription = this.ms.requestInformation(email, message, subject).subscribe(
      {
        next: val => {
          console.log(val);
          if (val) {
            this.notification = 'delivered';
            this.waitingForResponse = false;
          } else {
            this.notification = 'deliver failed';
            this.waitingForResponse = false;
          }},
        complete: () => {},
        error: (e) => {
          console.log(e);
          this.notification = 'deliver failed';
          this.waitingForResponse = false; }
      }
    );
    }
  }

  public getContact() {

    this.contactSubscription =  this.ms.requestContact().subscribe(
      {
        next: (contacts) => {
          let i = 0;
          // @ts-ignore
          while (i < contacts.length ) {
            this.contacts[i] = contacts[i];
            i++;
          }
          console.log('******************', this.contacts);
          this.contactObtained = true;
          setTimeout(() => {
            this.contactObtained = false;
          }, 3000);
        },
        complete: () => {},
        error: (e) => {
          console.log(e);
          this.contactObtained = false;
        }
      }
    );
  }

  public isEmail() {
    // console.log(this.contactEmail, this.lastname, this.firstname, this.email);
    return this.contactEmail.includes('@');
  }

  public getEmail(target) {
    this.contactEmail = target;
  }

  ngOnDestroy(): void {
    this.emailSubscription.unsubscribe();
    this.contactSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.getContact();
  }

}
