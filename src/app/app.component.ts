import {Component, OnInit} from '@angular/core';
import {SessionService} from './shared/SessionService';
import {isBoolean} from 'util';
import {LoginService} from './shared/LoginService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Wooster Brush';
  constructor(private ss: SessionService,
              private ls: LoginService) {}
  ngOnInit(): void {
    this.ss.loggedInAsAdmin().subscribe(
      {
        next: value => {
            this.ss.admin = value;
            this.ls.user = this.ss.getUserName();
          },
        complete: () => {},
        error: err => {
          console.log(err);
          this.ss.admin.success = false;
        }
      }
    );
    this.ss.loggedInAsOperator().subscribe({
      next: value => {
          this.ss.operator = value;
          this.ls.user = this.ss.getUserName();
        },
      complete: () => {},
      error: err => {
        console.log(err);
        this.ss.operator.success = false;
      }
    });
  }
}
