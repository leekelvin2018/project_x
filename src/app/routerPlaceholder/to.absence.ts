import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'absence-rerender',
  template: ''
})
export class ToAbsence{

  constructor(
    private router: Router
  ) {
    this.router.navigate(['manage/manage_absence'],{skipLocationChange: true});
  }



}
