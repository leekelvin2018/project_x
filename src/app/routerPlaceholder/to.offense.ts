import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'offense-rerender',
  template: ''
})
export class ToOffense {
  constructor(
    private router: Router
  ) {
    this.router.navigate(['manage/manage_offense'], {skipLocationChange: true});
  }

}
