import {Component, OnDestroy, OnInit} from '@angular/core';
import {AdminService} from '../shared/admin-service';
import {Observable, Subscription} from 'rxjs';
import {LoginService} from '../shared/LoginService';
import {Router} from '@angular/router';
import {SessionService} from '../shared/SessionService';


@Component({
  selector: 'app-administrator',
  templateUrl: 'administrator.component.html',
  styleUrls: ['administrator.component.css']
})
export class AdministratorComponent implements OnInit, OnDestroy {
  content;
  redirectCountDown = 3;
  private cleanTimeOut;
  adminPageSubscription: Subscription = new Subscription();
  adminPageObservable: Observable<any>;
  constructor(
    private adminService: AdminService,
    private ls: LoginService,
    private router: Router,
    private ss: SessionService
  ) {}

  ngOnInit(): void {
    this.adminPageObservable = this.adminService.test();
    if ( this.adminPageObservable == null) {
      console.log('unable to reach administrator page');
      return null;
    }
    this.adminPageSubscription = this.adminPageObservable.subscribe(
      {
        next: res => {
          this.content = res.a;
          console.log(res);
          if (this.content == null) {
            this.ls.logout();
          }
          },
        complete: () => {
          this.countDown();
        },
        error: (e) => {
          console.log(e);
          this.router.navigate(['login']);
        }
      }
    );
    this.ss.loggedInAsAdmin().subscribe(
      {
        next: value => {this.ss.admin = value; },
        complete: () => {},
        error: err => {
          console.log(err);
          this.ss.admin.success = false;
          this.ss.admin.message = null;
        }
      }
    );
  }

  public countDown() {
    if (this.redirectCountDown > 0) {
      this.cleanTimeOut =  setTimeout(() => {
          this.redirectCountDown--;
          this.countDown();
        },
        1000);
    } else {
      clearTimeout(this.cleanTimeOut);
      this.router.navigate(['home']);
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.cleanTimeOut);
  }
}
