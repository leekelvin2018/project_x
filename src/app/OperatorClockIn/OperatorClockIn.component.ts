import {Component, DoCheck, OnInit} from '@angular/core';
import {ReportService} from '../shared/report.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-operator-clock-in',
  templateUrl: 'OperatorClockIn.component.html',
  styleUrls: ['OperatorClockIn.component.css']
})
export class OperatorClockInComponent implements OnInit{
  public today = Date.now();

  public messageForOperator;
  public reportStatus = [false, false, false, false];
  private reportStatusSubscription = new Subscription();
  constructor(
    protected rs: ReportService
  ) {}

  ngOnInit(): void {
    setInterval(
      () => {
        this.today = Date.now();
      },
      1000
    );
    this.doCheck();
  }

  public onClockIn() {
    this.rs.clockIn().subscribe(
      {
        next: (val) => {
          console.log(val);
          // @ts-ignore
          this.messageForOperator = val.report;
        },
        complete: () => {
          this.doCheck();
        },
        error: (e) => {
          console.log(e);
        }
      }
    );
  }

  public onClockOut() {
    this.rs.clockOut().subscribe(
      {
        next: (val) => {console.log(val);
                        // @ts-ignore
          // @ts-ignore
                        this.messageForOperator = val.report; },
        complete: () => {
          this.doCheck();
        },
        error: (e) => {console.log(e); }
      }
    );
  }

  doCheck(): void {
    this.reportStatusSubscription =
      this.rs.getClockInOutStatus()
        .subscribe(
          {
            next: value => {
              if (value == null) {
                return;
              }
              this.reportStatus[0] = (value.firstin === 1);
              this.reportStatus[1] = (value.firstout === 1);
              this.reportStatus[2] = (value.secondin === 1);
              this.reportStatus[3] = (value.secondout === 1);
            },
            complete: () => {},
            error: err => {console.log(err); }
          }
        );
  }

}
