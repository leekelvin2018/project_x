import {Component, DoCheck, OnDestroy} from '@angular/core';
import {MessageService} from '../shared/message.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-contact',
  templateUrl: 'contact.component.html',
  styleUrls: ['contact.component.css']
})
export class ContactComponent implements OnDestroy {
  public name: string;
  public email: string;
  public message: string;
  public delivered = false;
  public delivering = false;
  public hasDeliverResult = false;
  public emailSubscription = new Subscription();
  public nameclick = false;
  public emailclick = false;
  public contentclick = false;
  constructor(protected ms: MessageService) {}

  public send() {

    this.hasDeliverResult = false;
    this.delivering = true;
    this.delivered = false;
    this.emailSubscription =
        this.ms.sendClientContact(this.name, this.email, this.message)
          .subscribe(
            {
              next: value => {
                console.log('delivered?', value);
                this.delivering = false;
                this.delivered = value;
                this.hasDeliverResult = true;
              },
              complete: () => {},
              error: err => {
                console.log(err);
                this.delivering = false;
                this.delivered = false;
                this.hasDeliverResult = true;
                },
            }
          );
  }

  public isEmail() {
    return this.email == null ? false : this.email.includes('@');
  }

  public hasName() {
    if (this.name == null || this.name === undefined) {
      return false;
    }
    return this.name.length != 0;
  }

  public hasContent() {
    if (this.message == null || this.message === undefined ) {
      return false;
    }
    return this.message.length !== 0;
  }

  public nameFieldClicked(event) {
    this.nameclick = true;
  }

  public emailFieldClicked() {
    this.emailclick = true;
  }

  public contentFieldClicked() {
    this.contentclick = true;
  }
  ngOnDestroy(): void {
    this.emailSubscription.unsubscribe();
  }


}
