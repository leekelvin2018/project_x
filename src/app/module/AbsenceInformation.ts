export  class AbsenceInformation {
  id;
  start_date;
  end_date;
  reason;
  status;
}
