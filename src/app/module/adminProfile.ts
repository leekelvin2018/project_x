export  class AdminProfile {
  first_name: string;
  last_name: string;
  profiles: Array<{authority, id, type}>;
}
