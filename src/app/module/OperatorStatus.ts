export class OperatorStatus {
  id;
  firstName;
  lastName;
  hasPendingAbsRequest;
  morningIn;
  morningOut;
  afternoonIn;
  afternoonOut;
}
