export class OperatorAbsenceDetail {
  start_date;
  end_date;
  reason;
  status;
  readableStatus;
}
