export class OperatorProfile {
  first_name: string;
  last_name: string;
  profiles: Array<{authority, id, type}>;
}
