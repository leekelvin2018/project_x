import {Component, OnDestroy, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../shared/LoginService';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: 'login-form.component.html',
  styleUrls: ['login-form.component.css']
})
export class LoginFormComponent implements OnInit, OnDestroy {
  private loginSubscription = new Subscription();
  public waitingForServerResponse = false;
  loginFormGroup: FormGroup;
  constructor(
    private fb: FormBuilder,
    public ls: LoginService,
    private router: Router) {}

  ngOnInit(): void {
    this.loginFormGroup = this.fb.group({
        username: ['',
          [Validators.required,
            Validators.minLength(5)]],
        password: ['',
          [Validators.required,
            Validators.minLength(6)]]
      },
      {validators: []}
    );
  }


  public onSubmit() {
    this.waitingForServerResponse = true;
    this.loginSubscription =
      this.ls.login(this.loginFormGroup.value)
        .subscribe({
          next: res => {
            if (res.success) {
              console.log('login done', res);
              // localStorage.setItem('user', JSON.stringify(this.loginFormGroup.value));
              this.ls.user = this.loginFormGroup.value;
              this.ls.loginFailure = false;
              const role = res.message;
              if (role.includes('ROLE_ADMIN')) {
                // localStorage.setItem('role', 'ROLE_ADMIN');
                this.router.navigate(['/administrator']);
              } else if (role.includes('ROLE_OPERATOR')) {
                // localStorage.setItem('role', 'ROLE_OPERATOR');
                this.router.navigate(['/operator']);
              }
            } else {
              console.log('failed:', res);
              this.ls.user = null;
              this.ls.loginFailure = true;
              // localStorage.clear();
              this.router.navigate(['/login']);
            }
            },
          complete: () => {this.waitingForServerResponse = false; },
          error: (e) => {
            console.log('login error:', e);
            this.waitingForServerResponse = false;
            console.log(e); }
        });
  }

  ngOnDestroy(): void {
    this.loginSubscription.unsubscribe();
  }
}
// {success: false, code: 200, message: "Not authorized resources"} if try to access stuff not suppose to
