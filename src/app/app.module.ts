import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginFormComponent} from './form/login-form.component';
import {HeaderComponent} from './header/header.component';
import {RouterModule} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {AppRoutingModule} from './app-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatCardModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatSidenavModule,
  MatListModule,
  MatFormFieldModule,
  MatInputModule,
  MatTableModule,
  MatTabsModule,
  MatTreeModule,
  MatChipsModule
} from '@angular/material';
import {HttpClientModule} from '@angular/common/http';
import {AdministratorComponent} from './administrator/administrator.component';
import {OperatorComponent} from './operator/operator.component';
import {RegisterComponent} from './register/register.component';
import {FaqComponent} from './faq/faq.component';
import {ContactComponent} from './contact/contact.component';
import {OperatorClockInComponent} from './OperatorClockIn/OperatorClockIn.component';
import {OperatorMessageComponent} from './operatorMessage/operatorMessage.component';
import {OperatorAbsenceComponent} from './operatorAbsence/operatorAbsence.component';
import {OperatorProfileComponent} from './OperatorProfile/operator-profile.component';
import { AdministratorClockInComponent} from './administratorClockIn/administrator-clock-in';
import {AdministratorMessageComponent} from './administratorMessage/administrator-message.component';
import {AdministratorProfileComponent} from './administratorProfile/administrator-profile.component';
import {ToAbsence} from './routerPlaceholder/to.absence';
import {ToOffense} from './routerPlaceholder/to.offense';
import {CalendarModule, DateAdapter} from 'angular-calendar';
import {adapterFactory} from 'angular-calendar/date-adapters/date-fns';
import {CalendarComponent} from './Calendar/calendar.component';
import {NgbModalModule, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FlatpickrModule } from 'angularx-flatpickr';
import {HomeSlideshowComponent} from './home/home-slideshow/home-slideshow.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {AngularMaterialModule} from './AngularMaterial/angular.material.module';
import {HashLocationStrategy, LocationStrategy} from '@angular/common';
@NgModule({
  declarations: [
    AppComponent,
    LoginFormComponent,
    HeaderComponent,
    HomeComponent,
    RegisterComponent,
    FaqComponent,
    ContactComponent,
    CalendarComponent,
    HomeSlideshowComponent,


    AdministratorComponent,
    AdministratorClockInComponent,
    AdministratorMessageComponent,
    AdministratorProfileComponent,

    OperatorComponent,
    OperatorClockInComponent,
    OperatorMessageComponent,
    OperatorAbsenceComponent,
    OperatorProfileComponent,

    ToAbsence,
    ToOffense
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AppRoutingModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    NgbModule,
    NgbModalModule,
    FlatpickrModule.forRoot(),

    HttpClientModule,

    AngularMaterialModule
  ],
  exports: [

  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy}
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }

