import {Component, OnDestroy, OnInit} from '@angular/core';
import {OperatorProfileService} from '../shared/operator.profile.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-operator-profile',
  templateUrl: 'operator-profile.component.html'
})
export class OperatorProfileComponent implements OnInit, OnDestroy {
  public placeholder;
  public profileSubscriber = new Subscription();
  public absRecordSubscriber = new Subscription();
  public offenseSubscriber = new Subscription();
  public penaltySubscriber = new Subscription();

  public displayedCols = ['start_date', 'end_date', 'Reason', 'Status'];

  public firsName;
  public lastName;
  public penalties;
  public absRecords: Array<{start_date: any,
  end_date: any,
  reason: any,
  status: number,
  readableStatus: string}>;
  public offenses;
  constructor(
    protected opProfile: OperatorProfileService
  ) {}
  public display() {
    this.profileSubscriber = this.opProfile.getProfile().subscribe(
      {next: (res) => {
        // @ts-ignore
        console.log(res.profiles[0].authority);
        this.firsName = res.first_name;
        this.lastName = res.last_name;
        },
      complete: () => {},
      error: (e) => {console.log(e); }}
    );
  }

  public absRecord() {
    this.absRecordSubscriber = this.opProfile.getAbsRecord().subscribe(
      {
        next: (res) => {
          console.log('absences: ', res instanceof Array);
          this.absRecords = res;
          if (res != null) {
            let length = 0;
            while (length < this.absRecords.length) {
              if (this.absRecords[length].status === -1) {
                this.absRecords[length].readableStatus = 'rejected';
              } else if (this.absRecords[length].status === 0) {
                this.absRecords[length].readableStatus = 'pending';
              } else {
                this.absRecords[length].readableStatus = 'approved';
              }
              length++;
            }

          }
        }
        ,
        complete: () => {},
        error: (e) => {console.log(e); }
      }
    );
  }

  public offenseCounts() {
    this.offenseSubscriber = this.opProfile.getOffenseCount().subscribe(
      {
        next: (res) => { this.offenses = res; },
        complete: () => {},
        error: (e) => {console.log(e); }
      }
    );
  }

  public getPenalty() {
    this.penaltySubscriber =
      this.opProfile.getTotalPenalty()
        .subscribe({
          next: value => {this.penalties = value; },
          complete: () => {},
          error: err => {console.log(err); }
        });
  }

  public ngOnInit(): void {
    this.display();
    this.absRecord();
    this.offenseCounts();
    this.getPenalty();
  }

  ngOnDestroy(): void {
    this.profileSubscriber.unsubscribe();
    this.absRecordSubscriber.unsubscribe();
    this.offenseSubscriber.unsubscribe();
    this.penaltySubscriber.unsubscribe();
  }
}
