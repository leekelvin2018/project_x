import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OperatorFinderService {
  private id;

  public getId() {
    return this.id;
  }

  public setId(id) {
    this.id = id;
  }

}
