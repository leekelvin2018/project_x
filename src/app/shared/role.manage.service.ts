import {Injectable} from '@angular/core';
import {SessionService} from './SessionService';

@Injectable({providedIn: 'root' })
export class RoleManageService {

  constructor(private ss: SessionService) {}

  public isAdmin() {
    return this.ss.admin.success;
  }

  public isOperator() {
    return this.ss.operator.success;
  }
}
