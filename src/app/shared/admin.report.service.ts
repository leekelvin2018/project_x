import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {SessionService} from './SessionService';

@Injectable({providedIn: 'root'})
export class AdminReportService {
  constructor(
    private httpClient: HttpClient,
    private ss: SessionService
  ) {}

  public clockIn() {
    return this.httpClient.post<{report: any}>(
      `${environment.URL}/report/admin/` + this.ss.getUserName(),
      null,
      {withCredentials: true}
    );
  }

  public clockOut() {
    return this.httpClient.post<{report: any}>(
      `${environment.URL}/report/out/` + this.ss.getUserName(),
      null,
      {withCredentials: true}
    );
  }
}
