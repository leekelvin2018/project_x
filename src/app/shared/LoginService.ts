import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';
import {Responses} from '../module/responses';
import {SessionService} from './SessionService';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  public user: string;
  public loginFailure = false; /* the intended functionality
  is to decide if show error message
  regarding login failure
  */
  constructor(
    private httpClient: HttpClient,
    private router: Router,
    private ss: SessionService) { }

  public login(user): Observable<Responses> {

    const userParam: HttpParams = new HttpParams()
      .append('username', user.username)
      .append('password', user.password);
    return this.httpClient.post<Responses>
    (`${environment.URL}/login`,
      userParam,
      {withCredentials: true});
  }

  public logout(): Observable<Responses> {
    this.user = null;
    this.loginFailure = false;
    localStorage.clear();
    this.ss.clear();
    this.router.navigate(['login']);
    return this.httpClient.post<Responses>
    (`${environment.URL}/logout`, null, {withCredentials: true});
  }
}

