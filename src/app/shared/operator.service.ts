import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {LoginService} from './LoginService';
import {Router} from '@angular/router';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OperatorService {
  constructor(
    protected httpClient: HttpClient,
    protected ls: LoginService,
    protected router: Router
  ) {}
  public test() {
    if (this.ls.user) {
      return this.httpClient.get<string>
      ( `${environment.URL}/op_test`,{withCredentials:true});
    }
    else {
      this.router.navigate(['home']);
    }
  }
}
