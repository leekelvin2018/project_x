import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Responses} from '../module/responses';

@Injectable({
  providedIn: 'root'
})
export  class SessionService {
  admin = new Responses();
  operator = new Responses();
  constructor(private router: Router,
              private httpClient: HttpClient) {
    console.log('check link', environment.URL);
  }

  loggedInAsAdmin() {
    console.log('check link in admin', environment.URL);
    return this.httpClient.get<Responses>(
      `${environment.URL}/check/admin`,
      {withCredentials: true}
    );
  }

  loggedInAsOperator() {
    return this.httpClient.get<Responses>(
      `${environment.URL}/check/operator`,
      {withCredentials: true}
    );
  }

  adminUN(): string {
    return this.admin.success ? this.admin.message : null;
  }

  operatorUN(): string {
    return this.operator.success ? this.operator.message : null;
  }

  getUserName(): string {
    return this.adminUN() ? this.adminUN() : this.operatorUN();
  }

  clear(): void {
    this.operator = new Responses();
    this.admin = new Responses();
  }
}
