import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {SessionService} from './SessionService';
import {EmailContact} from '../module/EmailContact';

@Injectable({providedIn: 'root'})
export class MessageService {

  constructor(
    private httpClient: HttpClient,
    private ss: SessionService
  ) {}
  // request information regarding mail delivery status
  public requestInformation(emailAddress, message, subjectt= 'No Subject') {
    return this.httpClient.post(
      `${environment.URL}/message/` + this.ss.getUserName(),
      {
        email: emailAddress,
        content: message,
        subject: subjectt
      },
      {withCredentials: true}
    );
  }

  public requestContact() {
    return this.httpClient.get<Array<EmailContact>>(
      `${environment.URL}/message`,
      {withCredentials: true}
    );
  }

  public sendClientContact(name, email, body) {
    console.log('send called');
    return this.httpClient.post<boolean>(
      `${environment.URL}/message`,
      {email: '',
            content: 'contact:' + email + '\n' + body + '\nfrom' + name,
            subject: '' },
      {withCredentials: true}
    );
  }

}
