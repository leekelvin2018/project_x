import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {SessionService} from './SessionService';
import {StatusReport} from '../module/statusReport';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  constructor(
    private httpClient: HttpClient,
    private ss: SessionService
  ) {

  }

  public clockIn() {
    return this.httpClient.post(
      `${environment.URL}/report/` + this.ss.getUserName(),
      null,
      {withCredentials: true}
    );
  }

  public clockOut() {
    return this.httpClient.post(
      `${environment.URL}/report/out/` + this.ss.getUserName(),
      null,
      {withCredentials: true}
    );
  }

  public getClockInOutStatus() {
    return this.httpClient.get<StatusReport>(
      `${environment.URL}/report/status/` + this.ss.getUserName(),
      {withCredentials: true}
    );
  }
}
