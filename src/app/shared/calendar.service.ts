import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {CalendarInformation} from '../module/CalendarInformation';

@Injectable({
  providedIn: 'root'
})
export class CalendarService {
  constructor(
    protected httpClient: HttpClient
  ) {}
  public getAllProvedAbsence() {
   return this.httpClient.get<Array<CalendarInformation>>(
      `${environment.URL}/absence/all_approved`,
      {withCredentials: true}
    );
  }
}
