import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'HasPendingRequest',
  pure: true
})
export class AbsencePipe implements PipeTransform{
  transform(operator: Array<{id, firstName, lastName, hasPendingAbsRequest, morningIn, morningOut, afternoonIn, afternoonOut }>,
            on: boolean): Array<{id, firstName, lastName, hasPendingAbsRequest, morningIn, morningOut, afternoonIn, afternoonOut }> {
    console.log('pipe status:', on);
    if (!on) {
      return operator;
    } else {
       return operator.filter(
        (item) => {
          return item.hasPendingAbsRequest;
      });
    }
  }

}
