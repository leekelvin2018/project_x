import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {OperatorProfile} from '../module/operatorProfile';
import {SessionService} from './SessionService';
import {OperatorAbsenceDetail} from '../module/operatorAbsenceDetail';

@Injectable({
  providedIn: 'root'
})
export  class OperatorProfileService {
  constructor(private httpClient: HttpClient,
              private ss: SessionService) {}

  public getProfile() {
    return this.httpClient.post<OperatorProfile>(
      `${environment.URL}/op_test/profile/` + this.ss.getUserName(),
      null,
      {withCredentials: true}
    );
  }

  public getAbsRecord() {
    return this.httpClient.get<Array<OperatorAbsenceDetail>>(
      `${environment.URL}/op_test/profile/` + this.ss.getUserName(),
      {withCredentials: true}
    );
  }

  public getOffenseCount() {
    return this.httpClient.get(
      `${environment.URL}/op_test/profile/offense/` + this.ss.getUserName(),
      {withCredentials: true}
    );
  }

  public getTotalPenalty() {
    return this.httpClient.get(
      `${environment.URL}/op_test/profile/penalties/` + this.ss.getUserName(),
      {withCredentials: true}
    );
  }
}
