import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {LoginService} from './LoginService';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  constructor(
    protected httpClient: HttpClient,
    protected ls: LoginService,
    protected router: Router
  ) {}

  public test() {
    if (this.ls.user) {
      return this.httpClient.get<string>(
        `${environment.URL}/ad_test`, {withCredentials: true});
    } else {
      this.router.navigate(['home']);
    }
  }
}
