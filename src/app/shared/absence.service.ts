import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {SessionService} from './SessionService';

@Injectable({providedIn: 'root'})
export class AbsenceService {
  constructor(
    private httpClient: HttpClient,
    private ss: SessionService
  ) {}
  public operatorRequestAbsence(startt, endd, reasonn) {
    return this.httpClient.post(
      `${environment.URL}/absence`,
    {
      username: this.ss.getUserName(),
      reason: reasonn,
      start: startt,
      end: endd
    },
    {withCredentials: true}
    );
  }
}
