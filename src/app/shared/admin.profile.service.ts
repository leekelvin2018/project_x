import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {SessionService} from './SessionService';
import {AdminProfile} from '../module/adminProfile';

@Injectable({
  providedIn: 'root'
})
export  class AdministratorProfileService {
  constructor(private httpClient: HttpClient,
              private ss: SessionService) {}

  public getProfile() {
    return this.httpClient.post<AdminProfile>(
      `${environment.URL}/ad_test/profile/` + this.ss.getUserName(),
      null,
      {withCredentials: true}
    );
  }



  public getOffenseCount() {
    return this.httpClient.get<number>(
      `${environment.URL}/ad_test/profile/offense/` + this.ss.getUserName(),
      {withCredentials: true}
    );
  }

}
