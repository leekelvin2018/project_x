import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {OperatorStatus} from '../module/OperatorStatus';
import {AbsenceInformation} from '../module/AbsenceInformation';
import {OffenseRecordInformation} from '../module/OffenseRecordInformation';

@Injectable({providedIn: 'root'})
export class ManageService {
  constructor(
    protected httpClient: HttpClient
  ) {

  }

  public getAllOperator() {
    return this.httpClient.get<Array<OperatorStatus>>(
      `${environment.URL}/ad_test/manage`,
      {withCredentials: true}
    );
  }

  public getAbsRecords(id: number) {
    return this.httpClient.get<Array<AbsenceInformation>>
    (
        `${environment.URL}/ad_test/manage/abs_record/` + id,
      {withCredentials: true}
      );
  }

  public resolveAbsRequest(AbsId: number, resolution: -1 | 1) {
    return this.httpClient.get<{report: string, error: string}>(
      `${environment.URL}/ad_test/manage/abs_record/resolve/`
      + AbsId + '/' + resolution,
      {withCredentials: true}
    );
  }

  public getOffenseRecords(eid) {
    return this.httpClient.get<
      Array<OffenseRecordInformation>
      >(
      `${environment.URL}/ad_test/manage/offense_record/` + eid,
      {withCredentials: true}
    );
  }

  public updateOffenseRecord(id, penalty, notes) {
    console.log('to server:', id, penalty, notes);
    return this.httpClient.post<{report, error}>(
      `${environment.URL}/ad_test/manage/offense_record`,
      {id, penalty, note: notes},
      {withCredentials: true}
    );
  }

  public helpClockInOut(id, when: 1|2|3|4, option: boolean) {
    let toServerOption = 0; // determine if clocking this person in or out
    if (option) {
      toServerOption = 1;
    } else {
      toServerOption = 0;
    }
    return this.httpClient.get(
      `${environment.URL}/ad_test/manage/clock_in_out/` + id + '/' + when + '/' + toServerOption,
      {withCredentials: true}
    );
  }
}
