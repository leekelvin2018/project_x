import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  private options = {
    headers: new HttpHeaders(
      { 'Content-Type': 'application/json',
      withCredentials: 'true'}) };

  constructor(
    protected httpClient: HttpClient
  ) {}

  public register(registry) {
    const regParam = new HttpParams()
      .append('username', registry.username)
      .append('password', registry.passwordGroup.password)
      .append('firstname', registry.firstName)
      .append('lastname', registry.lastName)
      .append('email', registry.email);
    return this.httpClient.post<boolean>(
      `${environment.URL}/register`, {
        username: regParam.get('username'),
        password: regParam.get('password'),
        firstname: regParam.get('firstname'),
        lastname: regParam.get('lastname'),
        email: regParam.get('email')
      }, {withCredentials: true }
    );
  }

  // demonstration of sending information through http's parameter
  public test(){
    this.httpClient.post(
      `${environment.URL}/register/testname/testpass/fn/ln/testemail`,
      null,
      {withCredentials: true }
    ).subscribe({
      next: (val) => {console.log(val); }
    });
  }

}
