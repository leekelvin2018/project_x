import {Component, OnInit} from '@angular/core';
import {LoginService} from '../shared/LoginService';
import {Router} from '@angular/router';
import {RoleManageService} from '../shared/role.manage.service';

@Component({
  selector: 'app-header',
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.css']
})
export class HeaderComponent implements  OnInit {
  public title = 'Wooster Brush';
  public home = 'Home';
  public login = 'login';
  public events: string[] = [];
  public opened: boolean;

  constructor(
    public ls: LoginService,
    private router: Router,
    public rm: RoleManageService) {
    if (ls.user == null) {
      router.navigate(['home']);
    }
  }

  public logoutChecker() {
    this.ls.logout().subscribe({
      next: (res) => {
        console.log(res);
         },
      complete: () => {},
      error: (e) => {console.log(e); }
    });
  }

  public ngOnInit(): void {
    const matToolbar = document.querySelector('mat-toolbar');
    console.log(matToolbar);
    document.onscroll = (e) => {
      console.log('scrolling?');
      if (window.scrollY > 0 && matToolbar.classList.contains('top')) {
        matToolbar.classList.remove('top');
        matToolbar.classList.add('moved');
      } else if (window.scrollY === 0 && matToolbar.classList.contains('moved')) {
        matToolbar.classList.remove('moved');
        matToolbar.classList.add('top');
      }
    };
  }
}
