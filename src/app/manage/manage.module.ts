import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ManageRoutingModule } from './manage-routing.module';
import { ManageAbsenceComponent } from './manage-absence/manage-absence.component';
import {ManageOffenseComponent, ManageOffenseDialogComponent} from './manage-offense/manage-offense.component';
import { ManageComponent } from './manage.component';
import {MatCheckboxModule, MatDialogModule, MatInputModule, MatTableModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AbsencePipe} from '../shared/AbsencePipe';

@NgModule({
  declarations: [ManageAbsenceComponent,
    ManageOffenseComponent,
    ManageComponent,
    ManageOffenseDialogComponent,
    AbsencePipe
  ],
  imports: [
    CommonModule,
    ManageRoutingModule,
    MatTableModule,
    MatDialogModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
  ],
  exports: [
    AbsencePipe
  ]
  ,
  bootstrap: [
    ManageOffenseDialogComponent
  ]
})
export class ManageModule { }
