import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {OperatorFinderService} from '../../shared/operator.finder.service';
import {ManageService} from '../../shared/manage.service';
import {Subscription} from 'rxjs';
import {ignore} from 'selenium-webdriver/testing';
import {Router} from '@angular/router';

@Component({
  selector: 'app-manage-absence',
  templateUrl: './manage-absence.component.html',
  styleUrls: ['./manage-absence.component.css']
})
export class ManageAbsenceComponent implements OnInit, OnDestroy {

  public id;
  protected getAllAbsRecordSubscription = new Subscription();
  public absRecord: Array<{id, start_date, end_date, reason, status }> = [];
  public noRecords = true;
  protected approveSubscription = new Subscription();
  protected rejectSubscription = new Subscription();
  public error = false;

  constructor(
    protected findOperator: OperatorFinderService,
    protected ms: ManageService,
    protected router: Router
  ) {

  }

  public approve(absId: number) {
    this.approveSubscription =
      this.ms.resolveAbsRequest(absId, 1).subscribe(
        {
          next: (report) => {
            if (report.report) {
              alert(report.report);
              this.absRecord.map((record) => {
                if (record.id === absId) {
                  record.status = 'approved';
                }
                return record;
              });
            } else {
              alert(report.error);
            }
            },
          complete: () => {},
          error: err => {console.log(err); }
        }
    );
  }

  public reject(absId: number) {
    this.rejectSubscription =
      this.ms.resolveAbsRequest(absId, -1).subscribe(
        {
          next: (report) => {
            if (report.report) {
              alert(report.report);
              this.absRecord.map((record) => {
                if (record.id === absId) {
                  record.status = 'rejected';
                }
                return record;
              });
            } else {
              alert(report.error);
            }
          },
          complete: () => {},
          error: err => {console.log(err); }
        }
      );
  }

  ngOnInit() {
    this.id = this.findOperator.getId();
    this.getThisOperatorAbsRequests();
  }

  public getThisOperatorAbsRequests() {
    this.getAllAbsRecordSubscription = this.ms.getAbsRecords(this.id).subscribe(
      {
        next: value => {
          this.error = false;
          if (value.length === 0 || value == null) {
            this.noRecords = true;
          } else {
            this.noRecords = false;
            this.absRecord = value;
            let index = 0;
            while (index < this.absRecord.length) {
              if (this.absRecord[index].status === 0) {
                this.absRecord[index].status = 'Pending';
              } else if (this.absRecord[index].status === -1) {
                this.absRecord[index].status = 'Rejected';
              } else {
                this.absRecord[index].status = 'Approved';
              }
              index++;
            }
          }
        },
        complete: () => {},
        error: err => {
          console.log(err, err.status >= 400);
          this.error = true;
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.approveSubscription.unsubscribe();
    this.rejectSubscription.unsubscribe();
  }
}
