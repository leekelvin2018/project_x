import {Component, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {OperatorFinderService} from '../../shared/operator.finder.service';
import {ManageService} from '../../shared/manage.service';
import {Subscription} from 'rxjs';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {FormBuilder, FormGroup} from '@angular/forms';

export interface DialogData {
  eid: number;
  penalty: number;
  summary: string;
}

@Component({
  selector: 'app-manage-offense',
  templateUrl: './manage-offense.component.html',
  styleUrls: ['./manage-offense.component.css']
})
export class ManageOffenseComponent implements OnInit, OnDestroy {

  public id;
  public offenseRecord: Array<{id, employee_id, penalty, behaviorsummary}> = [];
  public offenseRecordSubscription = new Subscription();
  public hasRecord = false;
  constructor(
    protected findOperator: OperatorFinderService,
    protected ms: ManageService,
    public dialog: MatDialog
  ) {

  }

  public openDialog(oid, penalty, summary) {
    // console.log('entry:', oid, penalty, summary);
    const dialogRef = this.dialog.open(ManageOffenseDialogComponent,
      {
        width: '500px',
        data: {id: oid, penalty: penalty, summary: summary}
      });
    dialogRef.afterClosed().subscribe(
      value => {
        // console.log('after exit:', value);

        if (value) {
          this.ms.updateOffenseRecord(value.id, value.penalty, value.summary)
            .subscribe(
              {
                next: vale => {console.log(vale); },
                complete: () => {
                  const itemToUpdate = this.offenseRecord.find( (item) => {
                    return item.id == value.id;
                  });
                  itemToUpdate.penalty = value.penalty;
                  itemToUpdate.behaviorsummary = value.summary;
                },
                error: err => {console.log(err); }
              }
            );
        }
      }
    );

  }

  ngOnInit() {
    this.id = this.findOperator.getId();
    this.getRecord();
  }

  public getRecord() {
    this.offenseRecordSubscription =  this.ms.getOffenseRecords(this.id).subscribe(
      {
        next: (value) => {
          this.hasRecord = (value.length !== 0);
          console.log(value);
          this.offenseRecord = value;
        },
        complete: () => {},
        error: err => {
          this.hasRecord = false;
          console.log(err);
        }
      }
    );
  }

  ngOnDestroy(): void {
    this.offenseRecordSubscription.unsubscribe();
  }
}

@Component({
  selector: 'app-offense-dialog',
  templateUrl: 'manage-offense-dialog.component.html',
  styleUrls: ['manage-offense.component.css']
})
export class ManageOffenseDialogComponent implements OnInit {
  public placeholder: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<ManageOffenseDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private fb: FormBuilder
  ) {
    this.placeholder = this.fb.group({
      placeholder1: [],
      placeholder2: []
    });
  }

  ngOnInit(): void {
    console.log('check after entrance:', this.data);
  }

  public cancel() {
    this.dialogRef.close(false);
  }

  public enforce() {
    this.dialogRef.close(this.data);
  }

}
