import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManageOffenseComponent } from './manage-offense.component';

describe('ManageOffenseComponent', () => {
  let component: ManageOffenseComponent;
  let fixture: ComponentFixture<ManageOffenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManageOffenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManageOffenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
