import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ManageOffenseComponent} from './manage-offense/manage-offense.component';
import {ManageAbsenceComponent} from './manage-absence/manage-absence.component';
import {ManageComponent} from './manage.component';

const routes: Routes = [
  {
    path: '',
    component: ManageComponent,
    children: [
      {
        path: 'manage_absence',
        component: ManageAbsenceComponent
      },
      {
        path: 'manage_offense',
        component: ManageOffenseComponent
      },

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageRoutingModule { }
