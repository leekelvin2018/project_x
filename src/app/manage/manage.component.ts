import {Component, OnDestroy, OnInit} from '@angular/core';
import {ManageService} from '../shared/manage.service';
import {Subscription} from 'rxjs';
import {OperatorFinderService} from '../shared/operator.finder.service';
import {Router} from '@angular/router';
import {OperatorStatus} from '../module/OperatorStatus';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class ManageComponent implements OnInit, OnDestroy {
  showPendingOnly = false;
  private operatorInformationSubscription = new Subscription();
  operators: Array<OperatorStatus> = [];
  private ClockInOutSubscription = new Subscription();
  constructor(
    private ms: ManageService,
    private findOperator: OperatorFinderService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getAllOperator();
  }
  // consider adding a pipe to show only people with pending request
  protected getAllOperator() {
    this.operatorInformationSubscription =
      this.ms.getAllOperator().subscribe(
        {
          next: value => {
            this.operators = value;
            // console.log(value);
          },
          complete: () => {},
          error: err => {
            console.log(err);
            this.router.navigate(['login']);
          }
        }
      );
  }

  public setId(id, absence) {
    this.findOperator.setId(id);
    if (absence) {
      this.router.navigate(['toAbs']);
    } else {
      this.router.navigate(['toOfs']);
    }

  }

  public helpClockInOut(id, option: 1|2|3|4) {
    const item = this.operators.find(
      (operator) => {
        return operator.id === id;
      }
    );
    let toServerOption = false;
    if (option === 1) {
      item.morningIn = !item.morningIn;
      toServerOption = item.morningIn;
    } else if (option === 2) {
      item.morningOut = !item.morningOut;
      toServerOption = item.morningOut;
    } else if (option === 3) {
      item.afternoonIn = !item.afternoonIn;
      toServerOption = item.afternoonIn;
    } else {
      item.afternoonOut = !item.afternoonOut;
      toServerOption = item.afternoonOut;
    }
    this.ClockInOutSubscription =
      this.ms.helpClockInOut(id, option, toServerOption)
      .subscribe(
        {
          next: value => {console.log(value); },
          complete: () => {},
          error: err => console.log(err)
        }
      );

    this.operators.map(
      (itemToUpdate) => {
        if (itemToUpdate.id === item.id) {
          return item;
        } else {return itemToUpdate; }
      });
  }

  ngOnDestroy(): void {
    this.ClockInOutSubscription.unsubscribe();
  }

}
