import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RegisterService} from '../shared/register.service';

@Component({
  selector: 'app-registry',
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.css']
})
export class RegisterComponent implements OnInit, OnDestroy{
  private registerSubscription = new Subscription();
  waitingForServer = false;
  registrationDone = false;
  hasResult = false;
  registerFormGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private rs: RegisterService) {
  }

  ngOnInit(): void {
    this.registerFormGroup = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      firstName: ['', [Validators.required, Validators.minLength(1)]],
      lastName: ['', [Validators.required, Validators.minLength(1)]],
      email: ['', [Validators.required, Validators.email]],
      passwordGroup: this.fb.group({
          password: ['', [Validators.required, Validators.minLength(6)]],
          confirmation: ['', [Validators.required, Validators.minLength(6)]]
        },
        {validators: [this.passwordValidator]})
    });
  }

  public checkPassword() {
    // @ts-ignore
    return document.getElementById('pw').value === document.getElementById('confirmation').value;
  }

  public passwordValidator(passwordGroup) {

    const{password, confirmation} = passwordGroup.value;
    return password === confirmation ? true : {notMatch: 'pw does not match'};
  }

  public onSubmit() {
    this.hasResult = false;
    this.waitingForServer = true;
    this.registerSubscription =
      this.rs.register(this.registerFormGroup.value)
        .subscribe({
          next: (val) => {
            console.log(typeof val, 'in reg com');
            this.hasResult = true;
            this.registrationDone = val;
          },
          complete: () => {this.waitingForServer = false; },
          error: (e) => {
            console.log(e);
            this.waitingForServer = false; }
        });
  }

  ngOnDestroy(): void {
    this.registerSubscription.unsubscribe();
  }
}
