import {NgModule} from '@angular/core';
import {Routes, RouterModule, CanActivate} from '@angular/router';
import {LoginFormComponent} from './form/login-form.component';
import {HomeComponent} from './home/home.component';
import {AdministratorComponent} from './administrator/administrator.component';
import {OperatorComponent} from './operator/operator.component';
import {RegisterComponent} from './register/register.component';
import {FaqComponent} from './faq/faq.component';
import {ContactComponent} from './contact/contact.component';
import {OperatorClockInComponent} from './OperatorClockIn/OperatorClockIn.component';
import {OperatorMessageComponent} from './operatorMessage/operatorMessage.component';
import {OperatorProfileComponent} from './OperatorProfile/operator-profile.component';
import {AdministratorClockInComponent} from './administratorClockIn/administrator-clock-in';
import {AdministratorMessageComponent} from './administratorMessage/administrator-message.component';
import {AdministratorProfileComponent} from './administratorProfile/administrator-profile.component';
import {OperatorAbsenceComponent} from './operatorAbsence/operatorAbsence.component';
import {ManageModule} from './manage/manage.module';
import {ToAbsence} from './routerPlaceholder/to.absence';
import {ToOffense} from './routerPlaceholder/to.offense';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'login',
    component: LoginFormComponent
  },
  {
    path: 'administrator',
    component: AdministratorComponent
  },
  {
    path: 'operator',
    component: OperatorComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'faq',
    component: FaqComponent
  },
  {
    path: 'contact',
    component: ContactComponent
  },
  {
    path: 'operatorClockIn',
    component: OperatorClockInComponent
  },
  {
    path: 'operatorMessage',
    component: OperatorMessageComponent
  },
  {
    path: 'operatorAbsence',
    component: OperatorAbsenceComponent
  },
  {
    path: 'operatorProfile',
    component: OperatorProfileComponent
  },
  {
    path: 'adminClockIn',
    component: AdministratorClockInComponent
  },
  {
    path: 'adminMessage',
    component: AdministratorMessageComponent
  },
  {
    path: 'adminProfile',
    component: AdministratorProfileComponent
  },
  {
    path: 'manage',
    loadChildren: '../app/manage/manage.module#ManageModule'
  },
  {
    path: 'toAbs',
    component: ToAbsence
  },
  {
    path: 'toOfs',
    component: ToOffense
  },
  {
    path: '**',
    redirectTo: 'home'
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
