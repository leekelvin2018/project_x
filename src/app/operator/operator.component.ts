import {Component, OnDestroy, OnInit} from '@angular/core';
import {OperatorService} from '../shared/operator.service';
import {LoginService} from '../shared/LoginService';
import {Router} from '@angular/router';
import {SessionService} from '../shared/SessionService';

@Component({
  selector: 'app-operator',
  templateUrl: 'operator.component.html',
  styleUrls: ['operator.component.css']
})
export class OperatorComponent implements OnInit, OnDestroy {
  public content;
  public redirectCountDown = 3;
  private cleanTimeOut;
  constructor(
    private ops: OperatorService,
    private ls: LoginService,
    private router: Router,
    private ss: SessionService
  ) {}
  public ngOnInit(): void {
    this.ops.test().subscribe({
        next: (res) => {
          console.log('check result:', res);
          // @ts-ignore
          this.content = res.a;
          if (this.content == null) {
            this.ls.logout();
          }
        },
        complete: () => {
          this.countDown();
        },
        error: (e) => {
          console.log(e);
          this.router.navigate(['login']);
        }
      }
    );
    this.ss.loggedInAsOperator().subscribe(
      {
        next: value => {this.ss.operator = value; },
        complete: () => {},
        error: err => {
          console.log(err);
          this.ss.operator.success = false;
          this.ss.operator.message = null;
        }
      }
    );
  }

  public countDown() {
    if (this.redirectCountDown > 0) {
     this.cleanTimeOut =  setTimeout(() => {
        this.redirectCountDown--;
        this.countDown();
      },
        1000);
    } else {
      clearTimeout(this.cleanTimeOut);
      this.router.navigate(['home']);
    }
  }

  ngOnDestroy(): void {
    clearTimeout(this.cleanTimeOut);
  }
}
