import {Component} from '@angular/core';
import {AbsenceService} from '../shared/absence.service';

@Component({
  selector: 'app-operator-absence',
  templateUrl: 'operatorAbsence.html',
  styleUrls: ['operatorAbsence.component.css']
})
export class OperatorAbsenceComponent {
  public validInput = true;
  public state = false;
  constructor(
    private as: AbsenceService
  ) {}

  public submitAbsenceRequest() {
    const startmonth = (document.getElementById('startmonth') as HTMLInputElement).value;
    const startday = (document.getElementById('startday') as HTMLInputElement).value;
    const endmonth = (document.getElementById('endmonth') as HTMLInputElement).value;
    const endday = (document.getElementById('endday') as HTMLInputElement).value;
    const startDate = startmonth + '-' + startday;
    const endDate = endmonth + '-' + endday;
    const reason = (document.getElementById('reason')as HTMLInputElement).value;
    if (
      !this.validateDateInfo( startmonth, startday, endmonth, endday) ||
      !this.notExceedingMaxDayOfMonth(startmonth, startday) ||
      !this.notExceedingMaxDayOfMonth(endmonth, endday) ||
      (startday > endday && startmonth > endmonth)) {
      console.log(startday, startmonth, endday, endmonth);
      this.validInput = false;
      return Error('invalid date input');
    } else {
      this.validInput = true;
      this.as.operatorRequestAbsence(startDate, endDate, reason)
        .subscribe(
          {
            next: (val) => {
              console.log(val);
              // @ts-ignore
              this.state = val;
            },
            complete: () => {},
            error: (e) => {
              console.log(e);
              this.state = false;
            }
          }
        );
    }
  }

  private notExceedingMaxDayOfMonth(m , d) {
    console.log(typeof d, typeof +d);
    if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) {
      return d <= 31;
    } else if (m == 4 || m == 6 || m == 9 || m == 11) {
      return d <= 30;
    } else {
      return d <= 28;
    }

  }

  private validateDateInfo(m, d, m2, d2) {
    console.log(m,d,m2,d2, m == m2, +d > +d2);
    if (m > m2) {
      console.log('wrong month');
      return false;
    } else if (m == m2 && +d > +d2) {
      console.log('wrong day');
      return false;
    } else if (+m < 1 || +m2 < 1 || +m > 12 || +m2 > 12 || +d < 1 || +d2 < 1) {
      console.log('day/month less than 1');
      return false;
    }
    console.log('nothing is worng with this checker');
    return true;
  }
}
