import {Component, OnDestroy, OnInit} from '@angular/core';

@Component({
  selector: 'app-home-slideshow',
  templateUrl: 'home-slideshow.component.html',
  styleUrls: ['./home-slideshow.component.css']
})
export class HomeSlideshowComponent implements OnInit, OnDestroy{
  protected slideIndex = 0;
  private destroyed = false;
  private timeout;
  public slides(test?) {

    let i;
    const slides = document.getElementsByClassName('mySlides');
    const dots = document.getElementsByClassName('dot');
    for (i = 0; i < slides.length; i++) {
      // @ts-ignore
      slides[i].style.display = 'none';
    }

    this.slideIndex++;
    if (this.slideIndex > slides.length) {this.slideIndex = 1; }
    // for (i = 0; i < dots.length; i++) {
    //   dots[i].className = dots[i].className.replace(' active', '');
    // }

    // @ts-ignore
    slides[this.slideIndex - 1].style.display = 'block';

    // dots[this.slideIndex - 1].className += ' active';
    this.timeout = setTimeout(this.slides.bind(this, 'from set'), 7000); // Change image every 2 seconds
  }

  ngOnInit(): void {
    this.slides();
  }

  ngOnDestroy(): void {
    console.log('trying to clear timeout?');
    clearTimeout(this.timeout);
    this.destroyed = true;
  }
}
